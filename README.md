# RobinForGood

_Este proyecto es una plataforma web de un sistema de donaciones que utiliza la tecnología Blockchain para tener un historial de registros de las donaciones realizadas. Se lleva a cabo con el framework Convector sobre una red de Hyperledger Fabric. Además se completa el escenario con Firebase para servicios de almacenamiento en la nube y autenticación de usuarios y Angular para la capa de presentación._

_En concreto, es un sistema que conecta donantes con personas que viven en situaciones de vulnerabilidad u ONGs. El donante publica una donación en el sistema que podrá ser solicitada por las ONGs o donatarios registrados. Una vez solicitada una donación, el donante que publicó la donación será capaz de contactar con los solicitantes y realizar el intercambio. Posteriormente al intercambio, la persona que haya recibido la donación podrá generar un token que recibirá el donante. Este token podrá gastarse como un descuento en una de las empresas asociadas. Por último, un administrador será capaz de eliminar los tokens gastados o los que hayan caducado._


_Este repositorio es el prototipo del proyecto, siendo tokenDB la carpeta con los recursos de Convector e Hyperledger Fabric y la caperta webapp los recursos de Angular y Firebase._

## TokenDB

Para desplegar la red de Hyperledger Fabric es necesario ejecutar los siguientes mandatos:

```
npm i
npm run env:clean
npm run env:restart
npm run cc:start -- token

npx lerna bootstrap
npx lerna run start --scope server --stream
```

Para ver el word state de CouchDB acceder a:
`http://localhost:5084/_utils/#database/ch1_example/_all_docs`


## Webapp

Para desplegar el servidor con la página web ejecutar:

`npm start` ó `ng serve` 

Acceder a la dirección: `http://localhost:4200/`
