import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/services/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-token-transfer',
  templateUrl: './token-transfer.component.html',
  styleUrls: ['./token-transfer.component.css']
})
export class TokenTransferComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  tokenData: any = {};
  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.restApi.getToken(this.id).subscribe((data: {}) => {
      this.tokenData = data;
    })
  }

  // Transfer token data
  updateEmployee() {
    if (window.confirm('Estás seguro/a, quieres transferirlo?')) {
      this.restApi.transferToken(this.id, this.tokenData).subscribe(data => {
        this.router.navigate(['/token-list'])
      })
    }
  }

}
