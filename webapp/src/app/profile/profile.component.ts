import { Component, OnInit, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../shared/services/auth.service.ts.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestApiService } from '../shared/services/rest-api.service';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	profileForm: FormGroup;
	RoleType: any = ['Donante', 'Donatario', 'ONG', 'Empresa'];

	constructor(
		public authService: AuthService,
		public ngZone: NgZone,
		public fb: FormBuilder,
		public restApi: RestApiService

	) { }
	ngOnInit() {
		this.authService.GetUserData();
		let ZIPCODE_PATTERN = /^[0-9]{5,5}$/;
		let PHONE_PATTERN = /^[0-9]{9,9}$/;
		let AGE_PATTERN = /^[0-9]{2,2}$/;
		this.profileForm = this.fb.group({
			'zipcode': ['', [
				Validators.required,
				Validators.pattern(ZIPCODE_PATTERN)
			]
			],
			'role': ['', [
				Validators.required
			]
			],
			'description': ['', [
				Validators.required
			]
			],
			'phone': ['', [
				Validators.required,
				Validators.pattern(PHONE_PATTERN)
			]
			],
			'age': ['', [
				Validators.required,
				Validators.pattern(AGE_PATTERN)
			]
			]
		});

	}

	get description() { return this.profileForm.get('description') }
	get zipcode() { return this.profileForm.get('zipcode') }
	get phone() { return this.profileForm.get('phone') }
	get role() { return this.profileForm.get('role') }
	get age() { return this.profileForm.get('age') }

	//Profile Data
	SetProfileData() {
		const userUpdate = this.authService.UpdateUser({
			role: this.role.value,
			zipcode: this.zipcode.value,
			description: this.description.value,
			phone: this.phone.value,
			age: this.age.value
		})
		this.registerParticipant();
		this.ResetProfileForm();
		return userUpdate;
	}
	ResetProfileForm() {
		this.profileForm.reset();
		Object.keys(this.profileForm.controls).forEach(key => {
			this.profileForm.controls[key].setErrors(null)
		});
		this.authService.GetUserData();
		
	}

	registerParticipant() {
		const user = JSON.parse(localStorage.getItem('user'));
		const participantDetails = { participant: { id: user.uid, role: this.role.value } };
		this.restApi.registerParticipant(participantDetails).subscribe((data: {}) => {
		})
	}
}
