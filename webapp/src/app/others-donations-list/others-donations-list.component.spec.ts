import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OthersDonationsListComponent } from './others-donations-list.component';

describe('OthersDonationsListComponent', () => {
  let component: OthersDonationsListComponent;
  let fixture: ComponentFixture<OthersDonationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OthersDonationsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OthersDonationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
