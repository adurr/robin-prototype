import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Donation } from '../shared/services/donation';
import { AuthService } from '../shared/services/auth.service.ts.service';
import { DonationService } from '../shared/services/donation.service';


@Component({
  selector: 'app-others-donations-list',
  templateUrl: './others-donations-list.component.html',
  styleUrls: ['./others-donations-list.component.css']
})
export class OthersDonationsListComponent implements OnInit {

  DonationData: any = [];

  constructor(
    public authService: AuthService,
    private donationApi: DonationService,
  ) {
    this.authService.GetUserData();
    this.donationApi.GetDonationList()
      .snapshotChanges().subscribe(donations => {
        donations.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          const owner = JSON.parse(localStorage.getItem('user'));

          let donation = a as Donation;
          let included = false;
          this.DonationData.forEach(item => {
            if (item.$key == donation.$key) {
              included = true;
            }
          })
          let requested = false;
          const donationArray = Object.values(donation);
          const ids= Object.values(donationArray[2]);
          if (ids.includes(owner.uid)) {
            requested = true;
          }
          if (donation.status != "Terminada" && donation.ownerID != owner.uid && !included && !requested) {
            this.DonationData.push(donation)
          }
        })
      })
  }

  ngOnInit() {
  }
  /* Submit Donation */
  request(e) {
    if (window.confirm('¿Seguro que quieres solicitar esta donación?')) {
      this.donationApi.Request(e.$key);
    }
  }
}
