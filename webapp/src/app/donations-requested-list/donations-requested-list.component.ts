import { Component, ViewChild } from '@angular/core';
import { Donation } from './../shared/services/donation';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { DonationService } from './../shared/services/donation.service';
import { AuthService } from '../shared/services/auth.service.ts.service';
import { RestApiService } from '../shared/services/rest-api.service';

@Component({
  selector: 'app-donations-requested-list',
  templateUrl: './donations-requested-list.component.html',
  styleUrls: ['./donations-requested-list.component.css']
})
export class DonationsRequestedListComponent {
  
  dataSource: MatTableDataSource<Donation>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  DonationData: any = [];
  displayedColumns: any[] = [
    'title',
    //'description',
    'type',
    'ownerName',
    'status',
    'created',
    'location',
    'action'
  ];

  constructor(
    private donationApi: DonationService, 
    private authService:AuthService,
    public restApi: RestApiService
  ) { 
    this.donationApi.GetDonationList()
    .snapshotChanges().subscribe(donations => {
        donations.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          const owner = JSON.parse(localStorage.getItem('user'));
          let donation= a as Donation;
          const donationArray = Object.values(donation);
          const ids= Object.values(donationArray[2]);
          let included=false;
          this.DonationData.forEach(item=>{
            if(item.$key == donation.$key){
              included = true;
            }
          })
          if( !included && ids.includes(owner.uid) && donation.status=="Solicitada"){
            this.DonationData.push(a as Donation)
          }
        })
        /* Data table */
        this.dataSource = new MatTableDataSource(this.DonationData);
        /* Pagination */
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 0);
    })
  }

  createToken(e){
    const user = JSON.parse(localStorage.getItem('user'));
    const tokenDetails = {"doneeID": user.uid , "ownerID": e.ownerID, "typeDonation":e.type, "donationID": e.$key};
    this.restApi.createToken(tokenDetails).subscribe((data: {}) => {
    })
    this.donationApi.FinishDonation(e.$key);
  }
}
