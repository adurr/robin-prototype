import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonationsRequestedListComponent } from './donations-requested-list.component';

describe('DonationsRequestedListComponent', () => {
  let component: DonationsRequestedListComponent;
  let fixture: ComponentFixture<DonationsRequestedListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonationsRequestedListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonationsRequestedListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
