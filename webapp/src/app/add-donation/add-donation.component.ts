import { Component, OnInit, ViewChild } from '@angular/core';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from "@angular/forms";
import { DonationService } from '../shared/services/donation.service';

@Component({
  selector: 'app-add-donation',
  templateUrl: './add-donation.component.html',
  styleUrls: ['./add-donation.component.css']
})
export class AddDonationComponent implements OnInit {

  public visible = true;
  public selectable = true;
  public removable = true;
  public addOnBlur = true;
  @ViewChild('chipList') chipList;
  @ViewChild('resetDonationForm') myNgForm;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  public selectedDonationType: string;
  public donationForm: FormGroup;
  public DonationType: any = ['Audiovisual','Cocina', 'Juguetes', 'Libros', 'Material educativo', 'Muebles', 'Ropa','Tecnología','Otro'];

  public pictureMessage = '';
  public dataForm = new FormData();
  public fileName = '';
  public publicURL = '';
  public percentage = 0;
  public finished = false;

  ngOnInit() {
    this.donationApi.GetDonationList();
    this.submitDonationForm();
  }

  constructor(
    public fb: FormBuilder,
    private donationApi: DonationService
  ) { }

  /* Reactive donation form */
  submitDonationForm() {
    this.donationForm = this.fb.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      type: ['', [Validators.required]],
      location: ['', [Validators.required]]
    })
  }
  
  /* Get errors */
  public handleError = (controlName: string, errorName: string) => {
    return this.donationForm.controls[controlName].hasError(errorName);
  }

  /* Date */
  formatDate(e) {
    var convertDate = new Date(e.target.value).toISOString().substring(0, 10);
    this.donationForm.get('created').setValue(convertDate, {
      onlyself: true
    })
  }

  /* Reset form */
  resetForm() {
    this.donationForm.reset();
    Object.keys(this.donationForm.controls).forEach(key => {
      this.donationForm.controls[key].setErrors(null)
    });
  }

  /* Submit donation */
  submitDonation() {
    if (this.donationForm.valid) {
      if(this.dataForm.get('picture')){
        this.uploadPicture();
      }else{
        this.donationApi.AddDonation(this.donationForm.value,this.publicURL);
        this.resetForm();
      }
    }
  }

  public pictureForm = new FormGroup({
    picture: new FormControl(null, Validators.required),
  });

  /* Event that is triggered when the image type input changes */
  public changePicture(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.pictureMessage = `Imagen preparada: ${event.target.files[i].name}`;
        this.fileName = event.target.files[i].name;
        this.dataForm.delete('picture');
        this.dataForm.append('picture', event.target.files[i], event.target.files[i].name)
      }
    } else {
      this.pictureMessage = 'No hay una imagen seleccionada';
    }
  }

  /* Upload the image to Cloud Storage */
  public uploadPicture() {
    let picture = this.dataForm.get('picture');
    let reference = this.donationApi.referenceCloudStorage(this.fileName);
    let task = this.donationApi.taskCloudStorage(this.fileName, picture);

    /* Change the percentage */
    task.percentageChanges().subscribe((percentage) => {
      this.percentage = Math.round(percentage);
      if (this.percentage == 100) {
        this.finished = true;
      }
    });
    reference.getDownloadURL().subscribe((URL) => {
      this.publicURL = URL;
      this.donationApi.AddDonation(this.donationForm.value,this.publicURL);
      this.resetForm();
      this.pictureMessage ='';
      this.dataForm.delete('picture');
      this.dataForm.append('picture','','');
    });
  }
}
