import { Component, OnInit } from '@angular/core';
import { DiscountService } from '../shared/services/discount.service';
import { Discount } from '../shared/services/discount';

@Component({
  selector: 'app-show-discounts',
  templateUrl: './show-discounts.component.html',
  styleUrls: ['./show-discounts.component.css']
})
export class ShowDiscountsComponent implements OnInit {

  DiscountData: any = [];

  constructor(
    private discountApi: DiscountService
  ) {
    this.discountApi.GetDiscountList()
      .snapshotChanges().subscribe(discounts => {
        discounts.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          let discount = a as Discount;
          let included = false;
          this.DiscountData.forEach(item => {
            if (item.$key == discount.$key) {
              included = true;
            }
          })
          if (discount.status != "Caducado" && !included) {
            this.DiscountData.push(discount);
          }
        })
      })
  }

  ngOnInit() {
  }

}
