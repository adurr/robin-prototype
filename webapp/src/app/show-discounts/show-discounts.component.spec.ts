import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDiscountsComponent } from './show-discounts.component';

describe('ShowDiscountsComponent', () => {
  let component: ShowDiscountsComponent;
  let fixture: ComponentFixture<ShowDiscountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowDiscountsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDiscountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
