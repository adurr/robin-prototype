import { Injectable } from '@angular/core';
import { Donation } from './donation';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})

export class DonationService {
  donationsRef: AngularFireList<any>;
  donationRef: AngularFireObject<any>;
  doneeArray: string[] = [];
  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage) { }

  /* Create donation */
  AddDonation(donation: Donation, pictureURL:string) {
    const owner = JSON.parse(localStorage.getItem('user'));
    this.donationsRef.push({
      title: donation.title,
      description: donation.description,
      type: donation.type,
      ownerID: owner.uid,
      ownerName: owner.displayName,
      status: "Publicado",
      created: new Date().toUTCString().substring(5, 25),
      modified: 0,
      location: donation.location,
      pictureURL:pictureURL
    })
      .catch(error => {
        this.errorMgmt(error);
      })
  }

  /* Get donation */
  GetDonation(id: string) {
    this.donationRef = this.db.object('donations-list/' + id);
    return this.donationRef;
  }

  /* Get donation list */
  GetDonationList() {
    this.donationsRef = this.db.list('donations-list');
    return this.donationsRef;
  }

  /* Update donation */
  UpdateDonation(id, donation: Donation) {
    const owner = JSON.parse(localStorage.getItem('user'));
    this.donationRef.update({
      title: donation.title,
      description: donation.description,
      type: donation.type,
      status: donation.status,
      modified: new Date().toUTCString().substring(5, 25),
      location: donation.location
    })
      .catch(error => {
        this.errorMgmt(error);
      })
  }

  /* Delete donation */
  DeleteDonation(id: string) {
    this.donationRef = this.db.object('donations-list/' + id);
    this.donationRef.remove()
      .catch(error => {
        this.errorMgmt(error);
      })
  }
  /* Request a donation */
  Request(donation: Donation) {
    this.donationRef = this.db.object('donations-list/' + donation);
    const user = JSON.parse(localStorage.getItem('user'));
    this.getExistingDonee(donation).then(success => {
      if (!this.doneeArray.includes(user.uid)) {
        this.doneeArray.push(user.uid);
      }
      this.donationRef.update({
        status: "Solicitada",
        donee: this.doneeArray
      }).catch(error => {
        this.errorMgmt(error);
      })
      delete this.doneeArray;
      this.doneeArray = [];
    }).catch(error => {
      this.errorMgmt(error);
    })
  }

  private getExistingDonee(donation: Donation): Promise<any> {
    this.donationRef = this.db.object('donations-list/' + donation);
    const user = JSON.parse(localStorage.getItem('user'));

    return new Promise((resolve, reject) => {
      this.donationRef.snapshotChanges().subscribe(data => {
        if (data.payload.val().donee) {
          data.payload.val().donee.forEach(id => {
            if (id != user.uid && !this.doneeArray.includes(id)) {
              this.doneeArray.push(id);
            }
          })
        } else {
          if (!this.doneeArray.includes(user.uid)) {
            this.doneeArray.push(user.uid);
          }
          this.donationRef.update({
            status: "Solicitada",
            donee: this.doneeArray
          }).catch(error => {
            this.errorMgmt(error);
          })
          delete this.doneeArray;
          this.doneeArray = [];
        }
        resolve(data);
      })
    })
  }
  // Error management
  private errorMgmt(error) {
    console.log(error)
  }

  /* Finish donation */
  FinishDonation(id: string) {
    this.donationRef = this.db.object('donations-list/' + id);
    this.donationRef.update({
      status: "Terminada",
      modified: new Date().toUTCString().substring(5, 25)
    })
      .catch(error => {
        this.errorMgmt(error);
      })
  }
  /* Task to upload a file */
  public taskCloudStorage(nameFile: string, data: any) {
    return this.storage.upload(nameFile, data);
  }

  /* File reference */
  public referenceCloudStorage(nameFile: string) {
    return this.storage.ref(nameFile);
  }

}