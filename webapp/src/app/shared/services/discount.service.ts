import { Injectable } from '@angular/core';
import { Discount } from './discount';
import { AngularFireList, AngularFireObject, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class DiscountService {
  discountsRef: AngularFireList<any>;
  discountRef: AngularFireObject<any>;
  constructor(
    private db: AngularFireDatabase,
    private storage: AngularFireStorage
  ) { }

  /* Create discount */
  AddDiscount(discount: Discount, pictureURL: string) {
    const owner = JSON.parse(localStorage.getItem('user'));
    this.discountsRef.push({
      title: discount.title,
      description: discount.description,
      type: discount.type,
      companyID: owner.uid,
      companyName: owner.displayName,
      status: "Publicado",
      created: new Date().toUTCString().substring(5, 25),
      expirationDate: discount.expirationDate,
      location: discount.location,
      pictureURL: pictureURL
    })
      .catch(error => {
        this.errorMgmt(error);
      })
  }
  /* Get discount list */
  GetDiscountList() {
    this.discountsRef = this.db.list('discounts-list');
    return this.discountsRef;
  }
  /* Get discount */
  GetDiscount(id: string) {
    this.discountRef = this.db.object('discounts-list/' + id);
    return this.discountRef;
  }

  /* Delete discount */
  DeleteDiscount(id: string) {
    this.discountRef = this.db.object('discounts-list/' + id);
    this.discountRef.remove()
      .catch(error => {
        this.errorMgmt(error);
      })
  }
  // Error management
  private errorMgmt(error) {
    console.log(error)
  }
  /* Task to upload a file */
  public taskCloudStorage(nameFile: string, data: any) {
    return this.storage.upload(nameFile, data);
  }

  /* File reference */
  public referenceCloudStorage(nameFile: string) {
    return this.storage.ref(nameFile);
  }
}
