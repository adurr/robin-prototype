export interface Donation {
    $key: string;
    title: string;
    description: string;
    type: string;
    ownerID: string;
    ownerName:string;
    donee:Array<string>;
    status:string;
    created: number;
    modified: number;
    location:string;
    pictureURL:string;
}
