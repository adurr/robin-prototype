import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Token } from '../token';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  // Define API
  apiURLtoken = 'http://localhost:8000/token';
  apiURLparticipant = 'http://localhost:8000/participant';
  constructor(
    private http: HttpClient
  ) { }
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  // HttpClient API get() method => Fetch tokens list
  getTokens(): Observable<Token> {
    return this.http.get<Token>(this.apiURLtoken + '/getAll')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  // HttpClient API get() method => Fetch token
  getToken(id): Observable<Token> {
    return this.http.get<Token>(this.apiURLtoken + '/getOne/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method => Fetch tokens by owner
  getTokensByOwner(ownerID): Observable<Token> {
    return this.http.get<Token>(this.apiURLtoken + '/getTokensByOwner/'+ ownerID, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API post() method => Create token
  createToken(token): Observable<Token> {
    return this.http.post<Token>(this.apiURLtoken + '/create', JSON.stringify(token), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  // HttpClient API put() method => transfer token
  transferToken(id:string, newownerID:string): Observable<Token> {
    const user = JSON.parse(localStorage.getItem('user'));
    let url = this.apiURLtoken + '/transfer/'+id+'/'+user.uid+'/'+newownerID;
    const tokenDetails = {};
    return this.http.put<Token>(url,JSON.stringify(tokenDetails), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API delete() method => Delete token
  deleteToken(id:string, empresaID:string){
    let url = this.apiURLtoken + '/delete/' + id + '/' + empresaID;
    return this.http.delete<Token>(url, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // Error handling 
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  // HttpClient API post() method => Register participant
  registerParticipant(data): Observable<any> {
    return this.http.post<any>(this.apiURLparticipant + '/register', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method => Fetch participants list
  getParticipants(): Observable<any> {
    return this.http.get<any>(this.apiURLparticipant + '/getAll')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // HttpClient API get() method => Fetch participant
  getParticipant(id): Observable<any> {
    return this.http.get<any>(this.apiURLparticipant + '/get/' + id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
}
