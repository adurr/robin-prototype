export interface User {
    uid: string;
    email: string;
    displayName: string;
    photoURL?: string;
    emailVerified: boolean;
    zipcode?: number;
    role?: string;
    phone?:number;
    description?:string;
    age?:number;
 }