export interface Discount {
    $key: string;
    title: string;
    description: string;
    type: string;
    companyID: string;
    companyName:string;
    status:string;
    created: number;
    expirationDate: number;
    location:string;
    pictureURL:string;
}
