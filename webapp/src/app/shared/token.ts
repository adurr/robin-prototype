export class Token {
    id: string;
    donationID:string;
    ownerID: string;
    doneeID: string;
    typeDonation:string;
    spent: boolean;
    created: number;
    modified: string;
    type:string;
}
