import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { SecureInnerPagesGuard } from './shared/guard/secure-inner-pages.guard';
import { AddDonationComponent } from './add-donation/add-donation.component';
import { EditDonationComponent } from './edit-donation/edit-donation.component';
import { DonationListComponent } from './donation-list/donation-list.component';
import { TokenTransferComponent } from './token-transfer/token-transfer.component';
import { TokenListComponent } from './token-list/token-list.component';
import { OthersDonationsListComponent } from './others-donations-list/others-donations-list.component';
import { ShowDiscountsComponent } from './show-discounts/show-discounts.component';



const appRoutes = [
    { path: '', component: WelcomeComponent,  pathMatch: 'full'},
    { path: 'navbar', component: NavbarComponent},
    { path: 'perfil', component: ProfileComponent,canActivate: [AuthGuard] },
    { path: 'inicio', component: WelcomeComponent},
    { path: 'quienessomos', component: AboutComponent},
    { path: 'login',component: LoginComponent, canActivate: [SecureInnerPagesGuard] },
    { path: 'register',component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
    { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
    { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [AuthGuard] },
    { path: 'add-donation',component: AddDonationComponent, canActivate: [AuthGuard] },
    { path: 'edit-donation/:id',component: EditDonationComponent, canActivate: [AuthGuard] },
    { path: 'donation-list',component: DonationListComponent, canActivate: [AuthGuard] },
    { path: 'transfer-token',component: TokenTransferComponent, canActivate: [AuthGuard] },
    { path: 'token-list',component: TokenListComponent, canActivate: [AuthGuard] },
    { path: 'other-donations-list',component: OthersDonationsListComponent, canActivate: [AuthGuard] },
    { path: 'show-discounts',component: ShowDiscountsComponent, canActivate: [AuthGuard] },

];
export const routing = RouterModule.forRoot(appRoutes);