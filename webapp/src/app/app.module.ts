import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { routing } from './app.routing';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AboutComponent } from './about/about.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { AddDonationComponent } from './add-donation/add-donation.component';
import { EditDonationComponent } from './edit-donation/edit-donation.component';
import { DonationListComponent } from './donation-list/donation-list.component';
import { TokenTransferComponent } from './token-transfer/token-transfer.component';
import { TokenListComponent } from './token-list/token-list.component';
import { DonationsRequestedListComponent } from './donations-requested-list/donations-requested-list.component';
import { OthersDonationsListComponent } from './others-donations-list/others-donations-list.component';

// Firebase services + enviorment module
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';

//Services
import { AuthService } from './shared/services/auth.service.ts.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DonationService } from './shared/services/donation.service';


/* Angular material */
import { AngularMaterialModule } from './material.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ModalComponent } from './modal/modal.component';
import { DiscountComponent } from './discount/discount.component';
import { ShowDiscountsComponent } from './show-discounts/show-discounts.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProfileComponent,
    WelcomeComponent,
    AboutComponent,
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    AddDonationComponent,
    EditDonationComponent,
    DonationListComponent,
    TokenTransferComponent,
    TokenListComponent,
    DonationsRequestedListComponent,
    OthersDonationsListComponent,
    ModalComponent,
    DiscountComponent,
    ShowDiscountsComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    FormsModule,
    NgbModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule
  ],
  providers: [AuthService, DonationService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [ModalComponent]
})
export class AppModule { }
