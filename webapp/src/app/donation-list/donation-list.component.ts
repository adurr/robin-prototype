import { Component, ViewChild } from '@angular/core';
import { Donation } from './../shared/services/donation';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { DonationService } from './../shared/services/donation.service';
import { AuthService } from '../shared/services/auth.service.ts.service';

@Component({
  selector: 'app-donation-list',
  templateUrl: './donation-list.component.html',
  styleUrls: ['./donation-list.component.css']
})
export class DonationListComponent {

  dataSource: MatTableDataSource<Donation>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  DonationData: any = [];
  displayedColumns: any[] = [
    //'$key',
    'publicURL',
    'title',
    'description',
    'type',
    //'ownerID',
    'status',
    'created',
    //'modified',
    'location',
    'action'
  ];
  constructor(private donationApi: DonationService, private authService:AuthService) {
    this.donationApi.GetDonationList()
    .snapshotChanges().subscribe(donations => {
        donations.forEach(item => {
          let a = item.payload.toJSON();
          a['$key'] = item.key;
          const owner = JSON.parse(localStorage.getItem('user'));

          let donation= a as Donation;
          let included=false;
          this.DonationData.forEach(item=>{
            if(item.$key == donation.$key){
              included = true;
            }
          })
          if(donation.ownerID==owner.uid && !included ){
            this.DonationData.push(donation)
          }
        })
        /* Data table */
        this.dataSource = new MatTableDataSource(this.DonationData);
        /* Pagination */
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 0);
    })
  }

  /* Delete */
  deleteDonation(index: number, e){
    if(window.confirm('¿Seguro que quieres borrar la donación?')) {
      const data = this.dataSource.data;
      data.splice((this.paginator.pageIndex * this.paginator.pageSize) + index, 1);
      this.dataSource.data = data;
      this.donationApi.DeleteDonation(e.$key)
    }
  }

  showContacts(e){
    this.authService.GetContacts(e.donee as []);
  }
}
