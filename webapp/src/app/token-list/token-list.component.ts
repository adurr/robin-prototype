import { Component, OnInit, ViewChild } from '@angular/core';
import { RestApiService } from "../shared/services/rest-api.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { AuthService } from '../shared/services/auth.service.ts.service';


@Component({
  selector: 'app-token-list',
  templateUrl: './token-list.component.html',
  styleUrls: ['./token-list.component.css']
})
export class TokenListComponent implements OnInit {
  Token: any = [];
  transferForm: FormGroup;

  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: any[] = [
    'tokenID',
    'created',
    'type',
    'action'
  ];

  constructor(
    public restApi: RestApiService,
    public fb: FormBuilder,
    public authService: AuthService
  ) { }

  ngOnInit() {
    this.loadTokens();
    this.transferForm = this.fb.group({
      EmpresaID: ['', [Validators.required]]
    })
    this.authService.GetUserData();
  }
  // Get tokens list
  loadTokens() {
    const user = JSON.parse(localStorage.getItem('user'));
    return this.restApi.getTokensByOwner(user.uid).subscribe((data: {}) => {
      this.Token = data;
      /* Data table */
      this.dataSource = new MatTableDataSource(this.Token);
      /* Pagination */
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
      }, 0);
    })
  }
  /* Get errors */
  public handleError = (controlName: string, errorName: string) => {
    return this.transferForm.controls[controlName].hasError(errorName);
  }
  //API REST PUT transfer
  transfer(token) {
    console.log(token);
    if (window.confirm('¿Estás seguro de gastar el token?')) {
      this.restApi.transferToken(token._id, this.transferForm.value.EmpresaID).subscribe(res => {
        this.loadTokens()
      });
    }
  }
  //API REST DELETE delete
  delete(token) {
    if (window.confirm('¿Estás seguro de borrar el token?')) {
      this.restApi.deleteToken(token._id, this.transferForm.value.EmpresaID).subscribe(res => {
        this.loadTokens()
      });
    }
  }
}
