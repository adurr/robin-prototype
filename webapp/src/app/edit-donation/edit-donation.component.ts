import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Location } from '@angular/common';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material';
import { DonationService } from './../shared/services/donation.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
@Component({
  selector: 'app-edit-donation',
  templateUrl: './edit-donation.component.html',
  styleUrls: ['./edit-donation.component.css']
})
export class EditDonationComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  @ViewChild('chipList') chipList;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  selectedDonationType: string;
  editDonationForm: FormGroup;
  DonationType: any = ['Electrónica', 'Juguetes', 'Ropa', 'Libros', 'Material educativo'];

  ngOnInit() {
    this.updateDonationForm();
  }

  constructor(
    public fb: FormBuilder,
    private location: Location,
    private donationApi: DonationService,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {
    var id = this.actRoute.snapshot.paramMap.get('id');
    this.donationApi.GetDonation(id).valueChanges().subscribe(data => {
      this.editDonationForm.setValue(data);
    })
  }

  /* Update form */
  updateDonationForm() {
    this.editDonationForm = this.fb.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      type: ['', [Validators.required]],
      ownerID: ['', [Validators.required]],
      location: ['', [Validators.required]],
      status: ['Modificada'],
      modified: ['', [Validators.required]]
    })
  }

  /* Get errors */
  public handleError = (controlName: string, errorName: string) => {
    return this.editDonationForm.controls[controlName].hasError(errorName);
  }

  /* Date */
  formatDate(e) {
    var convertDate = new Date(e.target.value).toISOString().substring(0, 10);
    this.editDonationForm.get('modified').setValue(convertDate, {
      onlyself: true
    })
  }

  /* Go to previous page */
  goBack() {
    this.location.back();
  }

  /* Submit Donation */
  updateDonation() {
    var id = this.actRoute.snapshot.paramMap.get('id');
    if (window.confirm('¿Estás seguro de actualizar?')) {
      this.donationApi.UpdateDonation(id, this.editDonationForm.value);
      this.router.navigate(['perfil']);
    }
  }

}
