import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { DiscountService } from '../shared/services/discount.service';
import { MatChipInputEvent } from '@angular/material';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})
export class DiscountComponent implements OnInit {

  public discountForm: FormGroup;
  public pictureMessage = '';
  public dataForm = new FormData();
  public fileName = '';
  public publicURL = '';
  public percentage = 0;
  public finished = false;

  public visible = true;
  public selectable = true;
  public removable = true;
  public addOnBlur = true;
  @ViewChild('chipList') chipList;
  @ViewChild('resetDonationForm') myNgForm;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  public selectedDiscountType: string;
  public DiscountType: any = ['Comida', 'Juguetes', 'Libros', 'Material educativo', 'Muebles', 'Ropa','Tecnología','Otro'];

  constructor(
    public fb: FormBuilder,
    private discountApi: DiscountService
  ) { }

  ngOnInit() {
    this.discountApi.GetDiscountList();
    this.submitDiscountForm();
  }

  /* Reactive discount form */
  submitDiscountForm() {
    this.discountForm = this.fb.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]],
      type: ['', [Validators.required]],
      location: ['', [Validators.required]],
      expirationDate: ['', [Validators.required]]
    })
  }

  /* Get errors */
  public handleError = (controlName: string, errorName: string) => {
    return this.discountForm.controls[controlName].hasError(errorName);
  }
  /* Date */
  formatDate(e) {
    var convertDate = new Date(e.target.value).toISOString().substring(0, 10);
    this.discountForm.get('expirationDate').setValue(convertDate, {
      onlyself: true
    })
  }

  /* Reset form */
  resetForm() {
    this.discountForm.reset();
    Object.keys(this.discountForm.controls).forEach(key => {
      this.discountForm.controls[key].setErrors(null)
    });
  }

  /* Submit discount */
  submitDiscount() {
    if (this.discountForm.valid) {
      if (this.dataForm.get('picture')) {
        this.uploadPicture();
      } else {
        this.discountApi.AddDiscount(this.discountForm.value, this.publicURL);
        this.resetForm();
      }
    }
  }
  public pictureForm = new FormGroup({
    picture: new FormControl(null, Validators.required),
  });

  /* Event that is triggered when the image type input changes */
  public changePicture(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.pictureMessage = `Imagen preparada: ${event.target.files[i].name}`;
        this.fileName = event.target.files[i].name;
        this.dataForm.delete('picture');
        this.dataForm.append('picture', event.target.files[i], event.target.files[i].name)
      }
    } else {
      this.pictureMessage = 'No hay una imagen seleccionada';
    }
  }

  /* Upload the image to Cloud Storage */
  public uploadPicture() {
    let picture = this.dataForm.get('picture');
    let reference = this.discountApi.referenceCloudStorage(this.fileName);
    let task = this.discountApi.taskCloudStorage(this.fileName, picture);

    /* Change the percentage */
    task.percentageChanges().subscribe((percentage) => {
      this.percentage = Math.round(percentage);
      if (this.percentage == 100) {
        this.finished = true;
      }
    });
    reference.getDownloadURL().subscribe((URL) => {
      this.publicURL = URL;
      this.discountApi.AddDiscount(this.discountForm.value, this.publicURL);
      this.resetForm();
      this.pictureMessage = '';
      this.dataForm.delete('picture');
      this.dataForm.append('picture', ' ', '');
    });
  }
}