#Comandos para probar la base de datos de Tokens y Participantes

npm i
npm run env:clean
npm run env:restart
npm run cc:start -- token

npx lerna bootstrap
npx lerna run start --scope server --stream

//Para ver el word state de CouchDB
http://localhost:5084/_utils/#database/ch1_example/_all_docs

//Participantes
//Registrar un participante
curl http://localhost:8000/participant/register -H "Content-Type: application/json" --request POST --data '{"participant":{"id":"2daab21d-db29-4901-b46d-e536e555555","role":"Donatario"}}'
curl http://localhost:8000/participant/register -H "Content-Type: application/json" --request POST --data '{"participant":{"id":"2daab21d-db29-4901-b46d-e536e33333","role":"Donante"}}'
curl http://localhost:8000/participant/register -H "Content-Type: application/json" --request POST --data '{"participant":{"id":"55555555555555555555555","role":"ONG"}}'
curl http://localhost:8000/participant/register -H "Content-Type: application/json" --request POST --data '{"participant":{"id":"11111111111111111111111","role":"Empresa"}}'
hurl invoke token participant_register '{"id":"22222222222222222222","role":"Donatario"}'

//Obtener un participante según su id
curl http://localhost:8000/participant/get/2daab21d-db29-4901-b46d-e536e33333 -H "Content-Type: application/json" --request GET
curl http://localhost:8000/participant/get/2daab21d-db29-4901-b46d-e536e555555 -H "Content-Type: application/json" --request GET

//Obtener lista de participantes
curl http://localhost:8000/participant/getAll -H "Content-Type: application/json" --request GET
hurl invoke token participant_getAll 

//Tokens
//Para crear un token es necesario tener el rol de donatario o de ONG y enviar el id del participante con los datos del token (tokenID, ownerID, doneeID y typeDonation)
curl http://localhost:8000/token/create -H "Content-Type: application/json" --request POST --data '{"doneeID":"55555555555555555555555","ownerID":"2daab21d-db29-4901-b46d-e536e17264567899","typeDonation":"Ropa", "donationID":"hgcsjxahdiqgxjabc"}'

//Obtener un token según su id
curl http://localhost:8000/token/getOne/ebe344ba-5cd2-4f83-99aa-8bfc1317bb6d -H "Content-Type: application/json" --request GET 

//Obtener las lista de tokens creados
curl http://localhost:8000/token/getAll -H "Content-Type: application/json" --request GET

//Devuelve según el propietario
curl http://localhost:8000/token/getTokensByOwner/2daab21d-db29-4901-b46d-e536e17264567899 -H "Content-Type: application/json" --request GET

//Borrar Token por ID, es necesario enviar el ID del participante que tiene que tener rol de Empresa
curl http://localhost:8000/token/delete/84f76a9b-6707-44d8-be86-9d721fd2b280/11111111111111111111111 -H "Content-Type: application/json" --request DELETE

//Transferir, tiene que tener rol de Empresa el newonerID y coincidir que el tokenID corresponde con su ownerID
curl http://localhost:8000/token/transfer/d0b8885a-39fd-4c21-8be9-b47a670ba084/2daab21d-db29-4901-b46d-e536e17264567899/11111111111111111111111 -H "Content-Type: application/json" --request PUT

//Devuelve los tokens de tipo de Ropa que NO son de ownerID: 2daab21d-db29-4901-b46d-e536e33333
curl http://localhost:8000/token/getTokensByType/2daab21d-db29-4901-b46d-e536e33333/Ropa -H "Content-Type: application/json" --request GET 



//Versión 0.1.6
curl http://localhost:8000/participant/get -H "Content-Type: application/json" --request GET --data '{"id":"2daab21d-db29-4901-b46d-e536e33333"}'
curl http://localhost:8000/token/getOne -H "Content-Type: application/json" --request GET --data '{"tokenID":"ebe344ba-5cd2-4f83-99aa-8bfc1317bb6d"}'
curl http://localhost:8000/token/getTokensByOwner -H "Content-Type: application/json" --request GET --data '{"ownerID":"2daab21d-db29-4901-b46d-e536e17264567899"}'
curl http://localhost:8000/token/delete -H "Content-Type: application/json" --request DELETE --data '{"participantID":"11111111111111111111111","tokenID":"ebe344ba-5cd2-4f83-99aa-8bfc1317bb6d"}'
curl http://localhost:8000/token/transfer -H "Content-Type: application/json" --request PUT --data '{"ownerID":"2daab21d-db29-4901-b46d-e536e17264567899","newownerID":"11111111111111111111111","tokenID":"8063f14b-a6c4-44e9-a7ab-05b2c53a15ce"}'
curl http://localhost:8000/token/getTokensByType -H "Content-Type: applicatByType -H "Content-Type: application/json" --request GET --data '{"typeDonation":"Ropa", "ownerID":"2daab21d-db29-4901-b46d-e536e33333"}' 

//Antigua versión para crear Tokens. Anterior a versión 0.1.6
curl http://localhost:8000/token/create -H "Content-Type: application/json" --request POST --data '{"id":"2daab21d-db29-4901-b46d-e536e555555","token":{"id":"2daab21d-db29-4901-b46d-e536e17264567899","typeDonation":"Clothes","owner":"OwnerTest","state":"activated","created":1574555350523,"modified":1574555350523}}'
curl http://localhost:8000/token/getOne/2daab21d-db29-4901-b46d-e536e1726564 -H "Content-Type: application/json" --request GET
//Devuelve los tokens de tipo de Clothes que no son de OwnerTest2
curl http://localhost:8000/token/getTokensByOwner/2daab21d-db29-4901-b46d-e536e17264567899 -H "Content-Type: application/json" --request GET
//Devuelve los tokens de tipo de Clothes que no son de OwnerTest2
curl http://localhost:8000/token/getTokensByType/Clothes/OwnerTest2 -H "Content-Type: application/json" --request GET 
//Borrar Token por ID, es necesario enviar el ID del participante que tiene que tener rol de Empresa
curl http://localhost:8000/token/delete/2daab21d-db29-4901-b46d-e536e1726564/11111111111111111111111 -H "Content-Type: application/json" --request DELETE
//Transferir token, aunque mejor usar delete
curl http://localhost:8000/token/transfer/2daab21d-db29-4901-b46d-e536e1726564/11111111111111111111111 -H "Content-Type: application/json" --request PUT --data '{"owner":"NewOwner"}'



//Para ver la Blockchain

Siendo el ID CONTAINER = 35f2d32d09f8  de  hyperledger/fabric-peer:1.4.0    peer0.org1.hurley.lab

docker exec -it 35f2d32d09f8 bash
Archivo con certificados, hashes y transacciones:
/var/hyperledger/production/ledgersData/chains/chains/ch1/blockfile_000000 
