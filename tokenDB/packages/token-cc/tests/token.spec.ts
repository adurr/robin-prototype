// tslint:disable:no-unused-expression
import { join } from 'path';
import { expect } from 'chai';
import * as uuid from 'uuid/v4';
import { MockControllerAdapter } from '@worldsibu/convector-adapter-mock';
import { ClientFactory, ConvectorControllerClient } from '@worldsibu/convector-core';
import 'mocha';

import { Token, TokenController } from '../src';

describe('Token', () => {
  let adapter: MockControllerAdapter;
  let tokenCtrl: ConvectorControllerClient<TokenController>;
  
  before(async () => {
    // Mocks the blockchain execution environment
    adapter = new MockControllerAdapter();
    tokenCtrl = ClientFactory(TokenController, adapter);

    await adapter.init([
      {
        version: '*',
        controller: 'TokenController',
        name: join(__dirname, '..')
      }
    ]);

    adapter.addUser('Test');
  });
  
  it('should create a default model', async () => {
    const modelSample = new Token({
      id: uuid(),
      typeDonation: 'Clothes',
      owner: 'OwnerTest',
      state: 'activated',
      created: Date.now(),
      modified: Date.now()
    });

    await tokenCtrl.$withUser('Test').create(modelSample);
  
    const justSavedModel = await adapter.getById<Token>(modelSample.id);
  
    expect(justSavedModel.id).to.exist;
  });

  it('should get all the tokens', async () => {
    let res = await tokenCtrl.getAll();
    console.log(JSON.stringify(res));
    expect(res.length).to.be.eq(1);
  });  

  it('should create a new token', async () => {
    const modelSample = new Token({
      id: uuid(),
      typeDonation: 'Electronics',
      owner: 'OwnerTest1',
      state: 'activated',
      created: Date.now(),
      modified: Date.now()
    });

    await tokenCtrl.$withUser('Test').create(modelSample);
  
    const justSavedModel = await adapter.getById<Token>(modelSample.id);
  
    expect(justSavedModel.id).to.exist;
  });

  it('should get all the tokens with 2 tokens', async () => {
    let res = await tokenCtrl.getAll();
    console.log(JSON.stringify(res));
    expect(res.length).to.be.eq(2);
  });  

 


});