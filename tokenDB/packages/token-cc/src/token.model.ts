import * as yup from 'yup';
import {
  ConvectorModel,
  Default,
  ReadOnly,
  Required,
  Validate
} from '@worldsibu/convector-core-model';

export class Token extends ConvectorModel<Token> {
  @ReadOnly()
  @Required()
  public readonly type = 'com.covalentx.token';

  @ReadOnly()
  @Required()
  @Validate(yup.string())
  public id: string;

  @Required()
  @Validate(yup.string())
  public donationID: string;

  @Required()
  @Validate(yup.string())
  public ownerID: string;

  @Required()
  @Validate(yup.string())
  public doneeID: string;

  @Required()
  @Validate(yup.string())
  public typeDonation: string;

  @Required()
  @Validate(yup.boolean())
  public spent: boolean;

  @ReadOnly()
  @Required()
  @Validate(yup.string())
  public created: string;

  @Required()
  @Validate(yup.string())
  public modified: string;
}
