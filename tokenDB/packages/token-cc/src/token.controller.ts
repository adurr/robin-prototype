import { ChaincodeTx } from '@worldsibu/convector-platform-fabric';
import * as yup from 'yup';
import * as uuid from 'uuid/v4';

import {
  Controller,
  ConvectorController,
  Invokable,
  Param
} from '@worldsibu/convector-core';

import { Token } from './token.model';
import { __await } from 'tslib';
import { Participant } from 'participant-cc';

@Controller('token')
export class TokenController extends ConvectorController<ChaincodeTx> {
  @Invokable()
  public async create(
    @Param(yup.string())
    ownerID: string,
    @Param(yup.string())
    doneeID: string,
    @Param(yup.string())
    donationID: string,
    @Param(yup.string())
    typeDonation: string
  ) {
    let participant = await Participant.getOne(doneeID);

    if (participant.role == "Donatario" || participant.role == "ONG") {
      const token = new Token({
        id: uuid(),
        ownerID: ownerID,
        doneeID: doneeID,
        donationID: donationID,
        typeDonation: typeDonation,
        spent: false,
        created: new Date().toUTCString().substring(5, 25),
        modified: new Date().toUTCString().substring(5, 25)
      });
      await token.save();
    }else{
      throw new Error(`The role ${participant.role} can't create a new Token`);
    }
  }

  @Invokable()
  public async getOne(
    @Param(yup.string())
    tokenID: string
  ) {
    const existing = await Token.getOne(tokenID);
    if (!existing || !existing.id) {
      throw new Error(`No token exists with that ID ${tokenID}`);
    }
    return await Token.getOne(tokenID);
  }

  @Invokable()
  public async getAll(
  ) {
    return await Token.getAll();
  }

  @Invokable()
  public async getTokensByOwner(
    @Param(yup.string())
    ownerID: string
  ) {
    var tokensWithOwnerID = [];
    const tokens = await Token.getAll();
    if (!tokens) {
      throw new Error(`No tokens exists`);
    }
    else {
      try {
        tokens.forEach(token => {
          if (token.ownerID && token.ownerID === ownerID) {
            tokensWithOwnerID.push(token);
          }
        })
      } catch (err) {
      }
    }
    if (!tokensWithOwnerID) {
      throw new Error(`No token exists with that Owner ${ownerID}`);
    }
    return tokensWithOwnerID;
  }

  @Invokable()
  public async transfer(
    @Param(yup.string()) tokenID: string,
    @Param(yup.string()) ownerID: string,
    @Param(yup.string()) newownerID: string

  ) {
    let participant = await Participant.getOne(newownerID);
    let token = await Token.getOne(tokenID);
    if (participant && participant.role == "Empresa" && token.ownerID == ownerID && !token.spent) {
      token.ownerID = newownerID;
      token.modified = new Date().toUTCString().substring(5, 25);
      token.spent = true;
      await token.save();
    }
  }

  @Invokable()
  public async delete(
    @Param(yup.string())
    tokenID: string,
    @Param(yup.string())
    participantID: string
  ) {
    const existing = await Token.getOne(tokenID);
    if (!existing || !existing.id) {
      throw new Error(`No token exists with that ID ${tokenID}`);
    }
    let participant = await Participant.getOne(participantID);
    if (participant.role == "Admin" && existing.spent) {
      if(existing.spent){
        existing.delete();
      }else{
        throw new Error(`The token with ID ${existing.id} can't be delete because it's not spent`);
      }
    } else{
      throw new Error(`The role ${participant.role} can't delete the Token`);
    }
  }
  
  @Invokable()
  public async getTokensByType(
    @Param(yup.string())
    ownerID: string,
    @Param(yup.string())
    typeDonation: string
  ) {
    var tokensOfTypeDonation = [];
    const tokens = await Token.getAll();
    if (!tokens) {
      throw new Error(`No tokens exists`);
    }
    else {
      tokens.forEach(token => {
        //Returns the Tokens by a DonationType with different owner
        if (token.ownerID && token.ownerID !== ownerID && token.typeDonation === typeDonation) {
          tokensOfTypeDonation.push(token);
        }
      })
    }
    if (!tokensOfTypeDonation) {
      throw new Error(`No token exists with that Owner ${ownerID}`);
    }
    return tokensOfTypeDonation;
  }
}