import { Request, Response } from 'express';
import { TokenControllerBackEnd, ParticipantControllerBackEnd } from '../convector';
import { Token } from 'token-cc';

export async function TokenController_create_post(req: Request, res: Response): Promise<void>{
    try{
        let params = req.body;
            res.status(200).send(await TokenControllerBackEnd
                .create(params.ownerID, params.doneeID, params.donationID, params.typeDonation));
            
    } catch(ex) {
        console.log('Error post TokenController_create', ex.stack);
        res.status(500).send(ex);
    }
}
export async function TokenController_getOne_get(req: Request, res: Response): Promise<void>{
    try{
        let {tokenID} = req.params;
        const tokenToReturn = new Token(await TokenControllerBackEnd.getOne(tokenID));
        res.status(200).send(tokenToReturn.toJSON());
    } catch(ex) {
        console.log('Error get TokenController_getOne', ex.stack);
        console.log(JSON.stringify(ex));
        res.status(500).send(ex);
    }
}
export async function TokenController_getAll_get(req: Request, res: Response): Promise<void>{
    try{
            res.status(200).send(await TokenControllerBackEnd
                .getAll());
            
    } catch(ex) {
        console.log('Error get TokenController_getAll', ex.stack);
        res.status(500).send(ex);
    }
}
export async function TokenController_getTokensByOwner_get(req: Request, res: Response): Promise<void>{
    try{
        let {ownerID} = req.params;
            res.status(200).send(await TokenControllerBackEnd
                .getTokensByOwner(ownerID));
            
    } catch(ex) {
        console.log('Error get TokenController_getTokensByOwner', ex.stack);
        res.status(500).send(ex);
    }
}
export async function TokenController_transfer_put(req: Request, res: Response): Promise<void>{
    try{
        let {tokenID, ownerID, newownerID} = req.params;
        await TokenControllerBackEnd.transfer(tokenID,ownerID,newownerID);
        
        const tokenToReturn = new Token(await TokenControllerBackEnd.getOne(tokenID));
        res.status(200).send(tokenToReturn.toJSON());
            
    } catch(ex) {
        console.log('Error put TokenController_transfer', ex.stack);
        res.status(500).send(ex);
    }
}
export async function TokenController_delete_delete(req: Request, res: Response): Promise<void>{
    try{
        let {tokenID, participantID} = req.params;
        const tokenToDelete = new Token(await TokenControllerBackEnd.getOne(tokenID));
        await TokenControllerBackEnd.delete(tokenID,participantID);
        res.status(200).send(tokenToDelete.toJSON());
    } catch(ex) {
        console.log('Error get TokenController_delete', ex.stack);
        console.log(JSON.stringify(ex));
        res.status(500).send(ex);
    }
}
export async function TokenController_getTokensByType_get(req: Request, res: Response): Promise<void>{
    try{
        let {ownerID, typeDonation} = req.params;
            res.status(200).send(await TokenControllerBackEnd
                .getTokensByType(ownerID,typeDonation));
            
    } catch(ex) {
        console.log('Error get TokenController_getTokensByType', ex.stack);
        res.status(500).send(ex);
    }
}
export async function ParticipantController_register_post(req: Request, res: Response): Promise<void>{
    try{
        let params = req.body;
            res.status(200).send(await ParticipantControllerBackEnd
                .register(params.participant));
            
    } catch(ex) {
        console.log('Error post ParticipantController_register', ex.stack);
        res.status(500).send(ex);
    }
}
export async function ParticipantController_get_get(req: Request, res: Response): Promise<void>{
    try{
        let {id} = req.params;
        res.status(200).send(await ParticipantControllerBackEnd
            .get(id));
        
    } catch(ex) {
        console.log('Error get ParticipantController_get', ex.stack);
        res.status(500).send(ex);
    }
}
export async function ParticipantController_getAll_get(req: Request, res: Response): Promise<void>{
    try{
        res.status(200).send(await ParticipantControllerBackEnd
            .getAll());
        
    } catch(ex) {
        console.log('Error get ParticipantController_getAll', ex.stack);
        res.status(500).send(ex);
    }
}