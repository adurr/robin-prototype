import * as express from 'express';
import { 
    TokenController_create_post,
    TokenController_getOne_get,
    TokenController_getAll_get,
    TokenController_transfer_put, 
    TokenController_getTokensByOwner_get,
    TokenController_getTokensByType_get,
    TokenController_delete_delete,
    ParticipantController_register_post,
    ParticipantController_get_get,
    ParticipantController_getAll_get} from './controllers'
export default express.Router()
.post('/token/create', TokenController_create_post)
.get('/token/getOne/:tokenID', TokenController_getOne_get)
.get('/token/getAll', TokenController_getAll_get)
.get('/token/getTokensByOwner/:ownerID', TokenController_getTokensByOwner_get)
.get('/token/getTokensByType/:ownerID/:typeDonation', TokenController_getTokensByType_get)
.put('/token/transfer/:tokenID/:ownerID/:newownerID', TokenController_transfer_put)
.delete('/token/delete/:tokenID/:participantID', TokenController_delete_delete)

.post('/participant/register', ParticipantController_register_post)
.get('/participant/get/:id', ParticipantController_get_get)
.get('/participant/getAll', ParticipantController_getAll_get)