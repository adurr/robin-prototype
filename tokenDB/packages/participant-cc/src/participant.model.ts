import * as yup from 'yup';
import {
  ConvectorModel,
  Default,
  ReadOnly,
  Required,
  Validate
} from '@worldsibu/convector-core-model';

export class Participant extends ConvectorModel<Participant> {

  @ReadOnly()
  @Required()
  public readonly type = 'com.covalentx.participant';

  @Required()
  @Validate(yup.string())
  public id: string;

  @Required()
  @Validate(yup.string())
  public role: string;
}
